class Level extends Phaser.Scene {

    constructor() {
        super({
            key: 'Level'
        });
        this.active=false;
    }

    preload() {
        this.load.image('stars', 'assets/stars.png');
        this.load.image('platform', 'assets/platform.png');
        this.load.image('ground', 'assets/ground.png');
        this.load.spritesheet('green_dino', 'assets/dino1.png', {frameWidth: 72, frameHeight: 31});
        this.load.spritesheet('blue_dino', 'assets/dino2.png', {frameWidth: 72, frameHeight: 31});
        this.load.spritesheet('purple_dino', 'assets/dino3.png', {frameWidth: 72, frameHeight: 31});
    }


    init(data) {
        this.player_colour = data.d;
    }


    create() {


        this.sky = this.add.tileSprite(500, 500, 1024, 1024, 'stars');


        let platforms = this.physics.add.staticGroup();
        let ground = this.physics.add.staticImage(400, 568, 'ground');

        platforms.create(600, 400, 'platform');
        platforms.create(50, 250, 'platform');
        platforms.create(750, 220, 'platform');


        //   The dinos and their settings
        let dino_config = {
            x: 5,
            y: 50,
            name: this.player_colour,
            facing_left: false,
            cursors: this.input.keyboard.createCursorKeys(),
            speed: 150,
            jump: 340
        };


        this.dino = new Player(this, dino_config);

        this.physics.add.collider(this.dino, platforms);
        this.physics.add.collider(this.dino, ground);

        this.dino.setCollideWorldBounds(true);

        this.i = 0;

    }

    update() {
        this.dino.move();

        if (this.i % 10 === 0)
        {
            this.sky.tilePositionX += 1;
        }
        this.i++;
    }

}