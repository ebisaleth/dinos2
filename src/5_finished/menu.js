
class Menu extends Phaser.Scene {

    constructor() {
        super({
            key: 'Menu'
        })
    }

    preload() {
        this.load.image('stars', 'assets/stars.png');
        this.load.image('platform', 'assets/platform.png');
        this.load.image('ground', 'assets/ground.png');
        this.load.spritesheet('green_dino', 'assets/dino1.png', {frameWidth: 72, frameHeight: 31});
        this.load.spritesheet('blue_dino', 'assets/dino2.png', {frameWidth: 72, frameHeight: 31});
        this.load.spritesheet('purple_dino', 'assets/dino3.png', {frameWidth: 72, frameHeight: 31});
    }

    create() {

        // we don't want physics in the selection screen! 
        this.physics.pause();

        this.add.tileSprite(500, 500, 1024, 1024, 'stars');
        this.text = this.add.text(400, 200, 'Select your dinosaur.', {
            fontFamily: 'Arial',
            fontSize: 52,
            color: '#ffffff'
        });
        
        // contrary to sprites, text objects are not aligned at the centre per default! how curious. 
        this.text.setOrigin(0.5, 0.5);


        // a little tween example to make the text float. 
        this.tweens.add({
            targets: this.text,
            y: 210,
            duration: 1000,
            ease: 'Linear',
            yoyo: true,
            repeat: -1
        });

        //   most of these properties are unnecessary because physics and movement are turned off.
        this.dino = new Player(this, {
            x: 400,
            y: 300,
            name: 'green_dino',
            facing_left: false,
            cursors: this.input.keyboard.createCursorKeys(),
            speed: 200,
            jump: 370
        });

        this.blue_dino = new Player(this, {
            x: 300,
            y: 300,
            name: 'blue_dino',
            facing_left: false,
            cursors: this.input.keyboard.createCursorKeys(),
            speed: 200,
            jump: 370
        });

        this.purple_dino = new Player(this, {
            x: 500,
            y: 300,
            name: 'purple_dino',
            facing_left: false,
            cursors: this.input.keyboard.createCursorKeys(),
            speed: 200,
            jump: 370
        });

        // animations 
        this.dinos = [this.dino, this.blue_dino, this.purple_dino];
        this.dinos.forEach(x => x.play(x.name + 'left'));
        
        //make them clickable and set the click callback 
        this.dinos.forEach(x => x.setInteractive());
        let scene = this;
        this.dinos.forEach(x =>x.on('pointerdown', function() {
            scene.select(x.name)
        }) )
    }

    // click callback
    select(dino_colour) {
        let scene = this;
        this.dinos.forEach(x => {
            scene.anims.remove(x.name + 'left');
            scene.anims.remove(x.name + 'right');
        });
        game.scene.stop('Menu');
        game.scene.start('Level', {d: dino_colour});
    }


}
