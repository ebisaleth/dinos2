class Level extends Phaser.Scene {

    constructor() {
        super({
            key: 'Level'
        });
    }


    preload() {
        this.load.image('stars', 'assets/stars.png');
        this.load.spritesheet('dino', 'assets/dino1.png', {frameWidth: 72, frameHeight: 31});
        this.load.image('ground', 'assets/ground.png');
    }


    create() {

        this.sky = this.add.tileSprite(500, 500, 1024, 1024, 'stars');

        let ground = this.physics.add.staticImage(400, 568, 'ground');

        this.dino = this.physics.add.sprite(200, 300, 'dino');

        this.physics.add.collider(this.dino, ground);

        this.i = 0;

    }

    update() {

        if (this.i % 10 === 0)
        {
            this.sky.tilePositionX += 1;
        }
        this.i++;
    }



}