class Player extends Phaser.Physics.Arcade.Sprite {


    //config: scene, x, y, name, facing_left, cursors;
    constructor(scene, config) {

        super(scene, config.x, config.y, config.name);

        this.scene = scene;
        this.cursors = config.cursors;
        this.facing_left = config.facing_left;
        this.speed = config.speed;
        this.jump = config.jump;
        this.name = config.name;

        this.scene.physics.world.enable(this);
        this.scene.add.existing(this);

        //debug
        console.log(this);

        //  Player physics properties. Give the little guy a slight bounce.
        this.setBounce(0.2);

        //  Our dino animations, turning, walking left and walking right.

        this.scene.anims.create({
            key: this.name + 'left',
            frames: this.scene.anims.generateFrameNumbers(this.name, {start: 0, end: 3}),
            frameRate: 10,
            repeat: -1
        });

        this.scene.anims.create({
            key: this.name + 'right',
            frames: this.scene.anims.generateFrameNumbers(this.name, {start: 4, end: 7}),
            frameRate: 10,
            repeat: -1
        });

        //  Input Events

    }

    move() {

        if (this.cursors.left.isDown) {
            this.setVelocityX(-this.speed);

            this.anims.play(this.name + 'left', true);
            this.facing_left = true;
        }
        else if (this.cursors.right.isDown) {
            this.setVelocityX(this.speed);

            this.anims.play(this.name + 'right', true);
            this.facing_left = false ;
        }
        else {
            this.setVelocityX(0);

            this.facing_left?this.setFrame(0):this.setFrame(5);
        }
        if (this.cursors.up.isDown && this.body.touching.down) {
            this.setVelocityY(-this.jump);
        }
    };


}