class Level extends Phaser.Scene {

    constructor() {
        super({
            key: 'Level'
        });
    }

    preload() {
        this.load.image('stars', 'assets/stars.png');
        this.load.image('ground', 'assets/ground.png');
        this.load.spritesheet('green_dino', 'assets/dino1.png', {frameWidth: 72, frameHeight: 31});
    }

    create() {
        this.sky = this.add.tileSprite(500, 500, 1024, 1024, 'stars');
        this.platforms = this.physics.add.staticGroup();
        this.ground = this.physics.add.staticImage(400, 568, 'ground');

        //   The dinos and their settings
        let dino_config = {
            x: 200,
            y: 50,
            name: 'green_dino',
            facing_left: false,
            cursors: this.input.keyboard.createCursorKeys(),
            speed: 150,
            jump: 340
        };


        this.dino = new Player(this, dino_config);

        this.physics.add.collider(this.dino, this.platforms);
        this.physics.add.collider(this.dino, this.ground);

        this.dino.setCollideWorldBounds(true);

    }

    update() {
        this.dino.move();
    }

}